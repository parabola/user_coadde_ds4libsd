# DS4LiBSD

It's a scripts for clean nonfree codes and files for any BSD,
then generate two separated sources:
* fully OS source named "DyfBOS-*".
* kernel and tools source named "DyfBKS-*"
  and add compability for the GNU C Library. 

---

## Fully Free BSDs and New Names:

All fully free OS and kernel gain with a new names, like:
* FreeBSD	-> DyfBOS-FreeIce and DyfBKS-FreeIce
* NetBSD	-> DyfBOS-LightNet and DyfBKS-LightNet
* OpenBSD	-> DyfBOS-LibreAqua and DyfBKS-LibreAqua
* DragonFly BSD	-> DyfBOS-AirBird and DyfBKS-AirBird

---

## Recursive Acronym Names:

All the names's a recursive acronym, like:
* DS4LiBSD (DS4LiBSD's scripts for liberate BSDs.)
* DyfBOS (DyfBOS's full"Y" "F"ree "B"erkeley "O"perating "S"ystem.)
* DyfBKS (DyfBKS's full"Y" "F"ree "B"erkeley "K"ernel "S"oftware.)
* The initial letter "D" mean DAVIANA.
* DAVIANA (DAVIANA's "A" ser"VI"ce "A"nd "N"ot d"A"emon.)

--

## DAVIANA logo and mascots:

DAVIANA is a new generic mascot that replace the Beastie daemon mascot,
It's a human girl, with cyan and white dress and serving a water drink.

* On DyfB*S-FreeIce uses DAVIANA mascot,
  with ice cubes on the drink.

* On DyfBOS-LightNet logo, is the same with opposing color,
  the red-orange to cyan-blue (#f26711 -> #119cf2) and uses
  DAVIANA mascot with the flag on the drink.

* On DyfBOS-LibreAqua, Puffy will be renamed to Puffree,
  without spines and replace with opposing color,
  the yellow to blue (#fff68e -> #8f98ff) or
  uses DAVIANA mascot with the Puffree inside in the drink.

* On DyfBOS-AirBird mascot, will be use generic bird with cyan color,
  or uses DAVIANA mascot with the generic bird.
